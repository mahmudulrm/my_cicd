#!/bin/bash
CONTAINER_IMAGE_NAME=$1
rm -rf /tmp/argocd
cd /tmp
git clone https://github.com/mahmudulrm/argocd.git
sed -i -e "s/mahmudulrm\/appnginx:.*/mahmudulrm\/$CONTAINER_IMAGE_NAME/g" /tmp/argocd/dev/deployment.yaml
cd /tmp/argocd
git add .
git commit -m "Update image to $IMAGE_TAG"
git remote set-url origin https://${GITHUB_USERNAME}:${GIT_TOKEN}@github.com/mahmudulrm/argocd.git
git config --global user.name "mahmudulrm"
git config --global user.email "mahmudulrm@gmail.com"
git push -u origin main
